import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
// import { SimpleNotificationsModule } from 'angular2-notifications';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { MainViewComponent } from './components/main-view/main-view.component';
import { AppRoutes } from './routes/routes.config';
import { AddlocationComponent } from './components/addlocation/addlocation.component';
import { LandinformationComponent } from './components/landinformation/landinformation.component';
import { BuildinginformationComponent } from './components/buildinginformation/buildinginformation.component';
import { PersonalinformationComponent } from './/components/personalinformation/personalinformation.component';
import { ApplicationStatesComponent } from './components/application-states/application-states.component';
import { SaveApplicationComponent } from './components/save-application/save-application.component';

import { BaseService } from './services/base.service';
import { SaveApplicationService } from './services/save-application.service';
import { StoreService } from './services/store.service';
// import { NotificationsMessageService } from './services/notifications.service';
import { AppBasicService } from './services/app-basic.service';
import { ViewPersonsService } from './services/view-persons.service';
import { ViewBuildingsService } from './services/view-buildings.service';
import { ViewLandsService } from './services/view-lands.service';

import { SuccessComponent } from './components/success/success.component';
import { FormsModule } from '@angular/forms';
// import { NotificationsService } from 'angular2-notifications';
import { SearchComponent } from './components/search/search.component';
import { SearchPersonComponent } from './components/search-person/search-person.component';
import { SearchBuildingComponent } from './components/search-building/search-building.component';
import { SearchLandComponent } from './components/search-land/search-land.component';
import { CommonModule } from '@angular/common';
// import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { ToastrModule } from 'ngx-toastr';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    MainViewComponent,
    AddlocationComponent,
    LandinformationComponent,
    BuildinginformationComponent,
    PersonalinformationComponent,
    ApplicationStatesComponent,
    SaveApplicationComponent,
    SuccessComponent,
    SearchComponent,
    SearchPersonComponent,
    SearchBuildingComponent,
    SearchLandComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    RouterModule.forRoot(AppRoutes),
    FormsModule,
    CommonModule,
    ToastrModule.forRoot()
    // SimpleNotificationsModule.forRoot()
  ],
  providers: [
    BaseService,
    SaveApplicationService,
    StoreService,
    // NotificationsMessageService,
    // NotificationsService,
    AppBasicService,
    ViewPersonsService,
    ViewBuildingsService,
    ViewLandsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
