export class API {
    SAVE_PERSONAL_APPLICATION = 'http://localhost:3000/api/save-personal-application';
    SAVE_BUILDING_APPLICATION = 'http://localhost:3000/api/save-building-application';
    SAVE_LAND_APPLICATION = 'http://localhost:3000/api/save-land-application';
    VIEW_PERSONS = 'http://localhost:3000/api/view-persons';
    VIEW_BUILDINGS = 'http://localhost:3000/api/view-buildings';
    VIEW_LANDS = 'http://localhost:3000/api/view-lands';
}