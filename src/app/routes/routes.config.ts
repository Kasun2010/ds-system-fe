import { Component } from '@angular/core';
import { Routes } from '@angular/router';
import { LoginComponent } from '../components/login/login.component';
import { MainViewComponent } from '../components/main-view/main-view.component';
import { AddlocationComponent } from '../components/addlocation/addlocation.component';
import { ApplicationStatesComponent } from '../components/application-states/application-states.component';
import { SaveApplicationComponent } from '../components/save-application/save-application.component';
import { SuccessComponent } from '../components/success/success.component';
import { SearchComponent } from '../components/search/search.component';
import { SearchPersonComponent } from '../components/search-person/search-person.component';
import { SearchBuildingComponent } from '../components/search-building/search-building.component';
import { SearchLandComponent } from '../components/search-land/search-land.component';
import { PersonalinformationComponent } from '../components/personalinformation/personalinformation.component';
import { BuildinginformationComponent } from '../components/buildinginformation/buildinginformation.component';
import { LandinformationComponent } from '../components/landinformation/landinformation.component';

export const AppRoutes: Routes = [
    { path: '', component: LoginComponent },
    { path: 'home', component: MainViewComponent },
    { path: 'addlocation', component: AddlocationComponent },
    { path: 'application-states', component: ApplicationStatesComponent },
    { path: 'save-application', component: SaveApplicationComponent },
    { path: 'success', component: SuccessComponent },
    { path: 'search', component: SearchComponent },
    { path: 'search-person', component: SearchPersonComponent },
    { path: 'search-building', component: SearchBuildingComponent },
    { path: 'search-land', component: SearchLandComponent },
    { path: 'personal-info', component: PersonalinformationComponent },
    { path: 'building-info', component: BuildinginformationComponent },
    { path: 'land-info', component: LandinformationComponent }
]

