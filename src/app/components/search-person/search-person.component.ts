import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppBasicService } from '../../services/app-basic.service';
import { ViewPersonsService} from '../../services/view-persons.service';

@Component({
  selector: 'app-search-person',
  templateUrl: './search-person.component.html',
  styleUrls: ['./search-person.component.css']
})
export class SearchPersonComponent implements OnInit {

  constructor(public router: Router,
              public appBasic:AppBasicService,
              public viewPersonDetails:ViewPersonsService) { }

  ngOnInit() {

    this.viewPersonDetails.getviewPersonsService().then(result=>{
      this.appBasic.viewPersons=result["data"];
      console.log(result['data']);
    }).catch(error=>{

    });
  }

  register() {
    this.router.navigateByUrl('/application-states');
  }

  search() {
    this.router.navigateByUrl('/search');
  }

  home() {
    this.router.navigateByUrl('/home');
  }

  searchPerson() {
    this.router.navigateByUrl('/search-person');
  }



}
