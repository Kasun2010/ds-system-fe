import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-application-states',
  templateUrl: './application-states.component.html',
  styleUrls: ['./application-states.component.css']
})
export class ApplicationStatesComponent implements OnInit {

  constructor(
    public router:Router
  ) { }

  ngOnInit() {
  }

  register(){
    this.router.navigateByUrl('/application-states');
  }

  search(){
    this.router.navigateByUrl('/search');
  }

  home(){
    this.router.navigateByUrl('/home');
  }
  addLocation(){
    this.router.navigateByUrl('/addlocation');
  }

  personalInfo(){
    this.router.navigateByUrl('/personal-info');
  }

  buildingInfo(){
    this.router.navigateByUrl('/building-info');
  }

  landInfo(){
    this.router.navigateByUrl('/land-info');
  }

}
