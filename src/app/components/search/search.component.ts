import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  constructor(  public router:Router ) { }

  ngOnInit() {
  }


  register(){
    this.router.navigateByUrl('/application-states');
  }

  search(){
    this.router.navigateByUrl('/search');
  }

  home(){
    this.router.navigateByUrl('/home');
  }

  searchPerson(){
    this.router.navigateByUrl('/search-person');
  }

  searchBuilding(){
    this.router.navigateByUrl('/search-building');
  }

  searchLand(){
    this.router.navigateByUrl('/search-land');
  }

}
