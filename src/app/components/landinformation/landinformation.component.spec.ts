import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LandinformationComponent } from './landinformation.component';

describe('LandinformationComponent', () => {
  let component: LandinformationComponent;
  let fixture: ComponentFixture<LandinformationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LandinformationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LandinformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
