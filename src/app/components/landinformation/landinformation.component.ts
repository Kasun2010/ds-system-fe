import { Component, OnInit } from '@angular/core';
import { StoreService } from '../../services/store.service';
import { BaseService } from '../../services/base.service';
import { API } from '../../constants/API';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-landinformation',
  templateUrl: './landinformation.component.html',
  styleUrls: ['./landinformation.component.css']
})
export class LandinformationComponent implements OnInit {


  api: API;
  constructor(public store: StoreService,
    public message: ToastrService,
    public router: Router,
    public baseService: BaseService) {
      this.api = new API();
     }

  ngOnInit() {
  }


  register() {
    this.router.navigateByUrl('/application-states');
  }

  search() {
    this.router.navigateByUrl('/search');
  }

  home() {
    this.router.navigateByUrl('/home');
  }

  formValidation(){
     if(this.store.landInfo.province == null){
      this.message.error("Please Enter province correctly", "LAND INFORMATION");
      return false;
    }else if(this.store.landInfo.district == null){
      this.message.error("Please Enter district correctly", "LAND INFORMATION");
      return false;
    }else if(this.store.landInfo.city == null){
      this.message.error("Please Enter city correctly", "LAND INFORMATION");
      return false;
    }else if(this.store.landInfo.address1 == null){
      this.message.error("Please Enter address 1 correctly", "LAND INFORMATION");
      return false;
    }else if(this.store.landInfo.address2 == null){
      this.message.error("Please Enter address 2 correctly", "LAND INFORMATION");
      return false;
    }else if(this.store.landInfo.address3 == null){
      this.message.error("Please Enter address 3 correctly", "LAND INFORMATION");
      return false;
    // }else if(this.store.landInfo.landType == null){
    //   this.message.error("Please Enter land type correctly", "LAND INFORMATION");
    //   return false;
    }else if(this.store.landInfo.size == null){
      this.message.error("Please Enter size correctly", "LAND INFORMATION");
      return false;
    }else if(this.store.landInfo.cultivations == null){
      this.message.error("Please Enter cultivations correctly", "LAND INFORMATION");
      return false;
    } else if(this.store.landInfo.licenseNumber == null){
      this.message.error("Please Enter license number correctly", "LAND INFORMATION");
      return false;
    } else if(this.store.landInfo.licenseType == null){
      this.message.error("Please Enter license type correctly", "LAND INFORMATION");
      return false;
    } else if(this.store.landInfo.ownerName == null){
      this.message.error("Please Enter owner name correctly", "LAND INFORMATION");
      return false;
    } else if(this.store.landInfo.ownerShip == null){
      this.message.error("Please Enter ownership correctly", "LAND INFORMATION");
      return false;
    } else if(this.store.landInfo.registeredDate == null){
      this.message.error("Please Enter registerd date correctly", "LAND INFORMATION");
      return false;
    } else if(this.store.landInfo.registrationNumber == null){
      this.message.error("Please Enter registration number correctly", "LAND INFORMATION");
      return false;
    } else if(this.store.landInfo.planNo == null){
      this.message.error("Please Enter plan number correctly", "LAND INFORMATION");
      return false;
    } else if(this.store.landInfo.lotNo == null){
      this.message.error("Please Enter lot number correctly", "LAND INFORMATION");
      return false;
    } else if(this.store.landInfo.boundaries == null){
      this.message.error("Please Enter boundaries correctly", "LAND INFORMATION");
      return false;
    }
    else{
      return true;
    }
  }

  saveApplication(){
    if(!this.formValidation()){

    }
    else{
      this.save({
        "landInformation": {
          "province":this.store.landInfo.province,
          "district":this.store.landInfo.district,
          "city":this.store.landInfo.city,
          "address1":this.store.landInfo.address1,
          "address2":this.store.landInfo.address2,
          "address3":this.store.landInfo.address3,
          "landType": this.store.landInfo.landType,
          "size": this.store.landInfo.size,
          "cultivations": this.store.landInfo.cultivations,
          "licenseNo": this.store.landInfo.licenseNumber,
          "licenseType": this.store.landInfo.licenseType,
          "ownerName": this.store.landInfo.ownerName,
          "ownerShip": this.store.landInfo.ownerShip,
          "registerdDate": this.store.landInfo.registeredDate,
          "regNo": this.store.landInfo.registrationNumber,
          "planNo": this.store.landInfo.planNo,
          "lotNo": this.store.landInfo.lotNo,
          "boundaries": this.store.landInfo.boundaries
        }
      }
      ).then(result => {
        this.router.navigateByUrl('/success');
      }).catch(error => {
        this.message.error('Something went wrong', "something went wrong while tring to save the application. please cehc your internet connection and try again");
      });
    }
  }

  save(data) {
    console.log(data);
    return new Promise((resolve, reject) => {
      this.baseService
        .post(this.api.SAVE_LAND_APPLICATION, data)
        .then(result => resolve(result))
        .catch(err => reject(err));
    });
  }
}
