import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ViewLandsService } from '../../services/view-lands.service';
import { AppBasicService } from '../../services/app-basic.service';

@Component({
  selector: 'app-search-land',
  templateUrl: './search-land.component.html',
  styleUrls: ['./search-land.component.css']
})
export class SearchLandComponent implements OnInit {

  constructor(public router: Router,
    public viewLandService: ViewLandsService,
    public appBasic: AppBasicService) { }

  ngOnInit() {
    this.viewLandService.getviewLandsService().then(result => {
      this.appBasic.viewLands = result['data'];
    }).catch(error => {

    });

  }


  register() {
    this.router.navigateByUrl('/application-states');
  }

  search() {
    this.router.navigateByUrl('/search');
  }

  home() {
    this.router.navigateByUrl('/home');
  }

}
