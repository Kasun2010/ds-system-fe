import { Component, OnInit } from '@angular/core';
import { StoreService } from '../../services/store.service';
import { BaseService } from '../../services/base.service';
import { API } from '../../constants/API';
import { Router } from '@angular/router';
import { PersonalInformationModel } from '../../models/personalinformation.model';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-personalinformation',
  templateUrl: './personalinformation.component.html',
  styleUrls: ['./personalinformation.component.css']
})
export class PersonalinformationComponent implements OnInit {

  api: API;
  constructor(public store: StoreService,
    public router: Router,
    public message: ToastrService,
    public baseService: BaseService) {
    this.api = new API();
  }

  ngOnInit() {
  }

  register() {
    this.router.navigateByUrl('/application-states');
  }

  search() {
    this.router.navigateByUrl('/search');
  }

  home() {
    this.router.navigateByUrl('/home');
  }

  formValidation() {
    if (this.store.personalInfo.province == null) {
      this.message.error("Please Enter province correctly", "PERSONAL INFORMATION");
      return false;
    } else if (this.store.personalInfo.district == null) {
      this.message.error("Please Enter district correctly", "PERSONAL INFORMATION");
      return false;
    } else if (this.store.personalInfo.city == null) {
      this.message.error("Please Enter city correctly", "PERSONAL INFORMATION");
      return false;
    } else if (this.store.personalInfo.address1 == null) {
      this.message.error("Please Enter address 1 correctly", "PERSONAL INFORMATION");
      return false;
    } else if (this.store.personalInfo.address2 == null) {
      this.message.error("Please Enter address 2 correctly", "PERSONAL INFORMATION");
      return false;
    } else if (this.store.personalInfo.address3 == null) {
      this.message.error("Please Enter address 3 correctly", "PERSONAL INFORMATION");
      return false;
    } else if (this.store.personalInfo.name == null) {
      this.message.error("Please Enter name correctly", "PERSONAL INFORMATION");
      return false;
    } else if (this.store.personalInfo.nic == null) {
      this.message.error("Please Enter nic correctly", "PERSONAL INFORMATION");
      return false;
    } else if (this.store.personalInfo.dob == null) {
      this.message.error("Please Enter date of birth correctly", "PERSONAL INFORMATION");
      return false;
    } else if (this.store.personalInfo.civilStatus == null) {
      this.message.error("Please Enter civil status correctly", "PERSONAL INFORMATION");
      return false;
    } else if (this.store.personalInfo.gender == null) {
      this.message.error("Please Enter gender correctly", "PERSONAL INFORMATION");
      return false;
    } else if (this.store.personalInfo.occupation == null) {
      this.message.error("Please Enter occupationcorrectly", "PERSONAL INFORMATION");
      return false;
    } else if (this.store.personalInfo.religion == null) {
      this.message.error("Please Enter religion correctly", "PERSONAL INFORMATION");
      return false;
    } else if (this.store.personalInfo.mobileNo == null) {
      this.message.error("Please Enter nmobile number correctly", "PERSONAL INFORMATION");
      return false;
    } else if (this.store.personalInfo.drivingLicenseNo == null) {
      this.message.error("Please Enter driving license number correctly", "PERSONAL INFORMATION");
      return false;
    } else if (this.store.personalInfo.size == null) {
      this.message.error("Please Enter size correctly", "PERSONAL INFORMATION");
      return false;
    } else if (this.store.personalInfo.birthPlace == null) {
      this.message.error("Please Enter birth place correctly", "PERSONAL INFORMATION");
      return false;
    } else if (this.store.personalInfo.charactor == null) {
      this.message.error("Please Enter character correctly", "PERSONAL INFORMATION");
      return false;
    } else if (this.store.personalInfo.disability == null) {
      this.message.error("Please Enter disability correctly", "PERSONAL INFORMATION");
      return false;
    } else if (this.store.personalInfo.nationality == null) {
      this.message.error("Please Enter nationality correctly", "PERSONAL INFORMATION");
      return false;
    } else if (this.store.personalInfo.passportNo == null) {
      this.message.error("Please Enter passport number correctly", "PERSONAL INFORMATION");
      return false;
    }
    else {
      return true;
    }
  }

  saveApplication() {
    if (!this.formValidation()) {
      console.log("hrere");
    }
    else {
      this.save({
        "personalInformation": {
          "province": this.store.personalInfo.province,
          "district": this.store.personalInfo.district,
          "city": this.store.personalInfo.city,
          "address1": this.store.personalInfo.address1,
          "address2": this.store.personalInfo.address2,
          "address3": this.store.personalInfo.address3,
          "name": this.store.personalInfo.name,
          "nic": this.store.personalInfo.nic,
          "dob": this.store.personalInfo.dob,
          "civilStatus": this.store.personalInfo.civilStatus,
          "gender": this.store.personalInfo.gender,
          "occupation": this.store.personalInfo.occupation,
          "religion": this.store.personalInfo.religion,
          "mobileNo": this.store.personalInfo.mobileNo,
          "drivingLicenseNo": this.store.personalInfo.drivingLicenseNo,
          "size": this.store.personalInfo.size,
          "birthPlace": this.store.personalInfo.birthPlace,
          "charactor": this.store.personalInfo.charactor,
          "disability": this.store.personalInfo.disability,
          "nationility": this.store.personalInfo.nationality,
          "highestEducation": this.store.personalInfo.highestEducation,
          "passportNo": this.store.personalInfo.passportNo
        }
      }
      ).then(result => {
        this.router.navigateByUrl('/success');
        this.store.personalInfo = new PersonalInformationModel();
      }).catch(error => {
          this.message.error('Something went wrong', "something went wrong while tring to save the application. please cehc your internet connection and try again");
        });
    }
  }

  save(data) {
    console.log(data);
    return new Promise((resolve, reject) => {
      this.baseService
        .post(this.api.SAVE_PERSONAL_APPLICATION, data)
        .then(result => resolve(result))
        .catch(err => reject(err));
    });
  }

}
