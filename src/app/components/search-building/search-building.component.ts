import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppBasicService } from '../../services/app-basic.service';
import { ViewBuildingsService } from '../../services/view-buildings.service';

@Component({
  selector: 'app-search-building',
  templateUrl: './search-building.component.html',
  styleUrls: ['./search-building.component.css']
})
export class SearchBuildingComponent implements OnInit {

  constructor(public router: Router,
    public appBasic: AppBasicService,
    public viewBuildingDetails: ViewBuildingsService) { }

  ngOnInit() {
    this.viewBuildingDetails.getviewBuildingsService().then(result => {
      this.appBasic.viewBuildings = result["data"];
      console.log(result['data']);
    }).catch(error => {

    });
  }

  register() {
    this.router.navigateByUrl('/application-states');
  }

  search() {
    this.router.navigateByUrl('/search');
  }

  home() {
    this.router.navigateByUrl('/home');
  }

}
