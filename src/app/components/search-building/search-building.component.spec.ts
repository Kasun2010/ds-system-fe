import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchBuildingComponent } from './search-building.component';

describe('SearchBuildingComponent', () => {
  let component: SearchBuildingComponent;
  let fixture: ComponentFixture<SearchBuildingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchBuildingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchBuildingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
