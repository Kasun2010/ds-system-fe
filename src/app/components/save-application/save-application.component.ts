import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { BaseService } from '../../services/base.service';
import { API } from '../../constants/API';
// import { NotificationsMessageService } from '../../services/notifications.service';
import { StoreService } from '../../services/store.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-save-application',
  templateUrl: './save-application.component.html',
  styleUrls: ['./save-application.component.css']
})
export class SaveApplicationComponent implements OnInit {

  api: API;
  constructor(public baseService: BaseService,
    public router: Router,
    public message: ToastrService,
    public store: StoreService) {
    this.api = new API();
  }

  ngOnInit() {
  }

  /**
   * validate the all 4 forms
   */
  formValidation() {
    if (this.store.location.coordinateX == null || this.store.location.coordinateY == null || this.store.location == null) {
      this.message.error("Please Enter coordinates correctly", "ADD LOCATION");
    }
    else if(this.store.location.address1 == null ||this.store.location.address2 == null || this.store.location.address3==null){
      this.message.error("Please Enter addresses correctly", "ADD LOCATION");
    }

   
   
  }

  saveApplication() {
    if (!this.formValidation) {

    }
    else {
      this.save(
        {
          "addLocation": {
            "coordinateX": this.store.location.coordinateX,
            "coordinateY": this.store.location.coordinateY,
            "coordinateZ": this.store.location.coordinateZ,
            "address1": this.store.location.address1,
            "address2": this.store.location.address2,
            "address3": this.store.location.address3
          },
          "landInformation": {
            "landType": this.store.landInfo.landType,
            "size": this.store.landInfo.size,
            "cultivations": this.store.landInfo.cultivations,
            "licenseNo": this.store.landInfo.licenseNumber,
            "licenseType": this.store.landInfo.licenseType,
            "ownerName": this.store.landInfo.ownerName,
            "ownerShip": this.store.landInfo.ownerShip,
            "registerdDate": this.store.landInfo.registeredDate,
            "regNo": this.store.landInfo.registrationNumber,
            "planNo": this.store.landInfo.planNo,
            "lotNo": this.store.landInfo.lotNo,
            "boundaries": this.store.landInfo.boundaries
          },
          "buildingInformation": {
            "wall": this.store.buildingInfo.wall,
            "roof": this.store.buildingInfo.roof,
            "floor": this.store.buildingInfo.floor,
            "sanctaryFacilities": this.store.buildingInfo.sanctaryFacilities,
            "Electricity": this.store.buildingInfo.electricity,
            "waterSource": this.store.buildingInfo.waterSource,
            "size": this.store.buildingInfo.size,
            "type": this.store.buildingInfo.type,
            "utilities": this.store.buildingInfo.utilities,
            "electoralRegNo": this.store.buildingInfo.electoralRegNo

          },

          "personalInformation": {
            "name": this.store.personalInfo.name,
            "nic": this.store.personalInfo.nic,
            "dob": this.store.personalInfo.dob,
            "civilStatus": this.store.personalInfo.civilStatus,
            "gender": this.store.personalInfo.gender,
            "occupation": this.store.personalInfo.occupation,
            "religion": this.store.personalInfo.religion,
            "mobileNo": this.store.personalInfo.mobileNo,
            "drivingLicenseNo": this.store.personalInfo.drivingLicenseNo,
            "size": this.store.personalInfo.size,
            "birthPlace": this.store.personalInfo.birthPlace,
            "charactor": this.store.personalInfo.charactor,
            "disability": this.store.personalInfo.disability,
            "nationility": this.store.personalInfo.nationality,
            "highestEducation": this.store.personalInfo.highestEducation,
            "passportNo": this.store.personalInfo.passportNo

          }



        }
      ).then(result => {
        this.router.navigateByUrl('/success');
      }).catch(error => {

        this.message.error('Something went wrong', "something went wrong while tring to save the application. please cehc your internet connection and try again");
      });
    }

  }
  save(data) {
    console.log(data);
    return new Promise((resolve, reject) => {
      this.baseService
        // .post(this.api.SAVE_APPLICATION, data)
        // .then(result => resolve(result))
        // .catch(err => reject(err));
    });
  }

}
