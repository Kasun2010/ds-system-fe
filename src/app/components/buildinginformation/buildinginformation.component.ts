import { Component, OnInit } from '@angular/core';
import { StoreService } from '../../services/store.service';
import { BaseService } from '../../services/base.service';
import { API } from '../../constants/API';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-buildinginformation',
  templateUrl: './buildinginformation.component.html',
  styleUrls: ['./buildinginformation.component.css']
})
export class BuildinginformationComponent implements OnInit {

  api: API;
  constructor(
    public store: StoreService,
    public router: Router,
    public message: ToastrService,
    public baseService: BaseService) {
    this.api = new API();
  }

  ngOnInit() {
  }

  register() {
    this.router.navigateByUrl('/application-states');
  }

  search() {
    this.router.navigateByUrl('/search');
  }

  home() {
    this.router.navigateByUrl('/home');
  }

  formValidation() {
    if (this.store.buildingInfo.province == null) {
      this.message.error("Please Enter province correctly", "BUILDING INFORMATION");
      return false;
    } else if (this.store.buildingInfo.district == null) {
      this.message.error("Please Enter district correctly", "BUILDING INFORMATION");
      return false;
    } else if (this.store.buildingInfo.city == null) {
      this.message.error("Please Enter city correctly", "BUILDING INFORMATION");
      return false;
    } else if (this.store.buildingInfo.address1 == null) {
      this.message.error("Please Enter address 1 correctly", "BUILDING INFORMATION");
      return false;
    } else if (this.store.buildingInfo.address2 == null) {
      this.message.error("Please Enter address 2 correctly", "BUILDING INFORMATION");
      return false;
    } else if (this.store.buildingInfo.address3 == null) {
      this.message.error("Please Enter address 3 correctly", "BUILDING INFORMATION");
      return false;
    }
    else if (this.store.buildingInfo.wall == null) {
      this.message.error("Please Enter wall type correctly", "BUILDING INFORMATION");
      return false;
    } else if (this.store.buildingInfo.roof == null) {
      this.message.error("Please Enter roof type correctly", "BUILDING INFORMATION");
      return false;
    } else if (this.store.buildingInfo.floor == null) {
      this.message.error("Please Enter floor type correctly", "BUILDING INFORMATION");
      return false;
    } else if (this.store.buildingInfo.wall == null) {
      this.message.error("Please Enter wall type correctly", "BUILDING INFORMATION");
      return false;
    } else if (this.store.buildingInfo.electricity == null) {
      this.message.error("Please select electricity connected or not correctly", "BUILDING INFORMATION");
      return false;
    } else if (this.store.buildingInfo.waterSource == null) {
      this.message.error("Please Enter water source correctly", "BUILDING INFORMATION");
      return false;
    } else if (this.store.buildingInfo.size == null) {
      this.message.error("Please Enter size correctly", "BUILDING INFORMATION");
      return false;
    } else if (this.store.buildingInfo.type == null) {
      this.message.error("Please Enter type correctly", "BUILDING INFORMATION");
      return false;
    } else if (this.store.buildingInfo.electoralRegNo == null) {
      this.message.error("Please Enter electoral registration number correctly", "BUILDING INFORMATION");
      return false;
    }
    else{
      return true;
    }

  }

  saveApplication() {
    if (!this.formValidation()) {

    }
    else {
      this.save({
        "buildingInfo": {
          "province":this.store.buildingInfo.province,
          "district":this.store.buildingInfo.district,
          "city":this.store.buildingInfo.city,
          "address1":this.store.buildingInfo.address1,
          "address2":this.store.buildingInfo.address2,
          "address3":this.store.buildingInfo.address3,
          "wall": this.store.buildingInfo.wall,
          "roof": this.store.buildingInfo.roof,
          "floor": this.store.buildingInfo.floor,
          "sanctaryFacilities": this.store.buildingInfo.sanctaryFacilities,
          "Electricity": this.store.buildingInfo.electricity,
          "waterSource": this.store.buildingInfo.waterSource,
          "size": this.store.buildingInfo.size,
          "type": this.store.buildingInfo.type,
          "utilities": this.store.buildingInfo.utilities,
          "electoralRegNo": this.store.buildingInfo.electoralRegNo
        }
      }
      ).then(result => {
        this.router.navigateByUrl('/success');
      }).catch(error => {
        this.message.error('Something went wrong', "something went wrong while tring to save the application. please cehc your internet connection and try again");
      });
    }
  }

  save(data) {
    console.log(data);
    return new Promise((resolve, reject) => {
      this.baseService
        .post(this.api.SAVE_BUILDING_APPLICATION, data)
        .then(result => resolve(result))
        .catch(err => reject(err));
    });
  }

}

