import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuildinginformationComponent } from './buildinginformation.component';

describe('BuildinginformationComponent', () => {
  let component: BuildinginformationComponent;
  let fixture: ComponentFixture<BuildinginformationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuildinginformationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuildinginformationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
