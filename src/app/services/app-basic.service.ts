import { Injectable } from '@angular/core';
import { ViewPersonsModel } from '../models/view-persons.model';
import { ViewBuildingsModel } from '../models/view-buildings.model';
import { ViewLandsModel } from '../models/view-lands.model';

@Injectable()
export class AppBasicService {

  viewPersons: Array<ViewPersonsModel> = new Array<ViewPersonsModel>();
  viewBuildings: Array<ViewBuildingsModel> = new Array<ViewBuildingsModel>();
  viewLands: Array<ViewLandsModel> = new Array<ViewLandsModel>();
  constructor() { }

}
