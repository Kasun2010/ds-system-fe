import { TestBed, inject } from '@angular/core/testing';

import { ViewPersonsService } from './view-persons.service';

describe('ViewPersonsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ViewPersonsService]
    });
  });

  it('should be created', inject([ViewPersonsService], (service: ViewPersonsService) => {
    expect(service).toBeTruthy();
  }));
});
