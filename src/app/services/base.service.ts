import { Injectable } from '@angular/core';
import Axios from 'axios';


@Injectable()
export class BaseService {

  public axios = Axios;
  constructor() { }

  /**
 *
 * This is for without token services
 * @param {string} path
 * @param {string} data
 * @returns
 * @memberof BaseService
 */
postWithoutToken(path, data) {
  return new Promise((resolve, reject) => {
    this.axios
      .post(path, data)
      .then(result => resolve(result.data))
      .catch(err => reject(err));
  });
}

get(path, data) {
  return new Promise((resolve, reject) => {
    this.setHeader();
    this.axios
      .get(path, { params: data })
      .then(result => resolve(result.data))
      .catch(err => reject(err));
  });
}

post(path, data) {
  return new Promise((resolve, reject) => {
    this.axios
      .post(path, data)
      .then(result => resolve(result.data))
      .catch(err => reject(err));
  });
}

  /**
   *
   *
   * @param {string} path
   * @param {string} data
   * @returns
   * @memberof BaseService
   */
  put(path, data) {
    return new Promise((resolve, reject) => {
      this.axios
        .put(path, data)
        .then(result => resolve(result.data))
        .catch(err => reject(err));
    });
  }

  /**
   *
   * This is for without token services
   * @param {string} path
   * @param {string} data
   * @returns
   * @memberof BaseService
   */
  getWithoutToken(path, data) {
    return new Promise((resolve, reject) => {
      this.axios
        .get(path, { params: data })
        .then(result => resolve(result.data))
        .catch(err => reject(err));
    });
  }

  setHeader() {
    this.axios.defaults.headers.common['token'] = localStorage.getItem('token');
    console.log('base service---> '+this.axios.defaults.headers.common['token'] );
  }

}
