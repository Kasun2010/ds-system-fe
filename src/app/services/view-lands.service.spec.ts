import { TestBed, inject } from '@angular/core/testing';

import { ViewLandsService } from './view-lands.service';

describe('ViewLandsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ViewLandsService]
    });
  });

  it('should be created', inject([ViewLandsService], (service: ViewLandsService) => {
    expect(service).toBeTruthy();
  }));
});
