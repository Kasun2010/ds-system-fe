import { Injectable } from '@angular/core';
import { BaseService } from './../services/base.service';
import { API } from './../constants/API';

@Injectable()
export class ViewBuildingsService {

  public api: API;
  constructor(public baseService:BaseService) {
    this.api = new API();
   }

  getviewBuildingsService() {
    return new Promise((resolve, reject) => {
      this.baseService
        .get(this.api.VIEW_BUILDINGS , {})
        .then(result => resolve(result))
        .catch(err => reject(err));
    });
  }
}
