import { Injectable } from '@angular/core';
import { LocationModel } from '../models/location.model';
import { LandInformationModel } from '../models/landinformation.model';
import { BuildingInformationModel } from '../models/buildinginformation.model';
import { PersonalInformationModel } from '../models/personalinformation.model';

@Injectable()
export class StoreService {

  location: LocationModel = new LocationModel();
  landInfo: LandInformationModel = new LandInformationModel();
  buildingInfo: BuildingInformationModel = new BuildingInformationModel();
  personalInfo: PersonalInformationModel = new PersonalInformationModel();

  constructor() { }

}
