import { TestBed, inject } from '@angular/core/testing';

import { AppBasicService } from './app-basic.service';

describe('AppBasicService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AppBasicService]
    });
  });

  it('should be created', inject([AppBasicService], (service: AppBasicService) => {
    expect(service).toBeTruthy();
  }));
});
