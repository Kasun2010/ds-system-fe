import { TestBed, inject } from '@angular/core/testing';

import { ViewBuildingsService } from './view-buildings.service';

describe('ViewBuildingsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ViewBuildingsService]
    });
  });

  it('should be created', inject([ViewBuildingsService], (service: ViewBuildingsService) => {
    expect(service).toBeTruthy();
  }));
});
