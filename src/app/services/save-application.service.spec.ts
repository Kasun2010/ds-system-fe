import { TestBed, inject } from '@angular/core/testing';

import { SaveApplicationService } from './save-application.service';

describe('SaveApplicationService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SaveApplicationService]
    });
  });

  it('should be created', inject([SaveApplicationService], (service: SaveApplicationService) => {
    expect(service).toBeTruthy();
  }));
});
