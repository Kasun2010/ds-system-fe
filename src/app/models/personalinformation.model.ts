export class PersonalInformationModel {
    province: String;
    district: String;
    city: String;
    address1: String;
    address2: String;
    address3: String;
    name: String;
    nic: String;
    dob: Date;
    civilStatus: String;
    gender: String;
    occupation: String;
    religion: String;
    mobileNo: String;
    drivingLicenseNo: String;
    size: String;
    birthPlace: String;
    charactor: String;
    disability: String;
    nationality: String;
    highestEducation: String;
    passportNo: String;
}