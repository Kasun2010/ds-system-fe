export class ViewLandsModel {
    province: String;
    district: String;
    city: String;
    type: String;
    owner: String;
}