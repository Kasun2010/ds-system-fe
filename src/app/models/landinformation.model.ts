export class LandInformationModel {
    province: String;
    district: String;
    city: String;
    address1: String;
    address2: String;
    address3: String;
    landType: String;
    size: String;
    cultivations: String;
    licenseNumber: Number;
    licenseType: String;
    ownerName: String;
    ownerShip: String;
    registeredDate: String;
    registrationNumber: String;
    planNo: String;
    lotNo: String;
    boundaries: String;

    constructor() {
        this.licenseNumber = 0;

    }
}