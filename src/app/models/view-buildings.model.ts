export class ViewBuildingsModel {
    province: String;
    city: String;
    district: String;
    size: String;
    elect_reg_no: String;
}