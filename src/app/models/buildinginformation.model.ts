export class BuildingInformationModel {
    province: String;
    district: String;
    city: String;
    address1: String;
    address2: String;
    address3: String;
    wall: String;
    roof: String;
    floor: String;
    sanctaryFacilities: String;
    electricity: String;
    waterSource: String;
    size: String;
    type: String;
    utilities: String;
    electoralRegNo: String;
}