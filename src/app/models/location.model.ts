export class LocationModel {
    coordinateX:Number;
    coordinateY:Number;
    coordinateZ:Number;
    address1: String;
    address2: String;
    address3: String;
    rateNo: Number;
    constructor() {
        this.rateNo = 0;
    }
}